---
title: "About"
#description: ""

menu:
  main:
    weight: 1
---

The research unit will conduct highly sensitive international joint experiments utilizing liquid xenon detectors, to elucidate dark matter, which constitutes the majority of the universe's mass yet remains shrouded in mystery.

In addition, as part of our future plans to enhance sensitivity in our searches, we will be developing a new liquid xenon detector featuring a quartz sealed vessel and a novel photodetector.
Through close collaboration with particle theorist and by harnessing synergy with neutrino research, we aim to pave the way for the next generation of dark matter research and advanced radiation detection technology.

## Members

- Yoshitaka ITOW (ISEE/NAIAS, Professor)
- Shingo KAZAMA (KMI/NAIAS, Associated Professor)
- Junji HISANO (KMI/NAIAS Professor)
- Masatoshi KOBAYASHI (KMI/NAIAS, Designated Assistant Professor)
- Marc SCHUMANN (University of Freiburg, Professor)

{{< figure src="../../images/members/group.jpeg" link="../../images/members/group_sm.jpeg"  loading="lazy" >}}
