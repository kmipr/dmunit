---
date: 2022-10-31T09:00:00+09:00
#description: "La grande halle"
featured_image: "PMT_array_perspective-1-scaled-2-1536x1152.jpeg"
tags: ["physics", "XENONnT"]
title: "The PRL paper of XENONnT was selected as Editor's Choice !"
---

The new result "Search for New Physics in Electronic Recoil Data from XENONnT" was published in the Physical Review Letter, and was selected as Editor's Choice.

<!--more-->

## Additional Information

- Name of the journal: Physical Review Letters (PRL)
- Title: "Search for New Physics in Electronic Recoil Data from XENONnT", E.Aprile et al. (XENON Collaboration), Physical Review Letter 129, 161805 (2022)
- Website: [Physical Review Letters (PRL)](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.129.161805)
- PDF: [Physical Review Letters (PRL)](https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.129.161805)