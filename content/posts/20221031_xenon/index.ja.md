---
date: 2022-10-31T09:00:00+09:00
#description: "La grande halle"
featured_image: "PMT_array_perspective-1-scaled-2-1536x1152.jpeg"
tags: ["physics", "XENONnT"]
title: "XENONnTの研究成果が Physical Review Letter の Editor's Choice に選出"
---

暗黒物質国際創生ユニット代表の伊藤好孝（いとう よしたか）宇宙地球環境研究所/国際高等研究機構教授らのXENONnTに関する研究成果「Search for New Physics in Electronic Recoil Data from XENONnT」が物理学レタージャーナル Physical Review Letter に掲載され、Editor's Choiceに選ばれました。

<!--more-->

Physical Review Letters（PRL）は、世界最高峰の物理学レタージャーナルであり、米国物理学会（APS）の基幹刊行物です。1958年以来、ノーベル賞受賞者をはじめとするあらゆる物理学分野の著名な研究者による最先端の研究を掲載し、物理学の知識の向上と普及というAPSの使命に貢献しています。ユニットメンバーである風間慎吾（かざま しんご）素粒子宇宙起源研究所/国際高等研究機構准教授は、XENONグループ解析コーディネーターとして結果をとりまとめ、本論文のCorresponding authorとなっています。

## 掲載情報

- 掲載ジャーナル: Physical Review Letters (PRL)
- タイトル: "Search for New Physics in Electronic Recoil Data from XENONnT", E.Aprile et al. (XENON Collaboration), Physical Review Letter 129, 161805 (2022)
- ウェブサイト: [Physical Review Letters (PRL)](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.129.161805)（外部サイト）
- PDF版: [Physical Review Letters (PRL)](https://journals.aps.org/prl/pdf/10.1103/PhysRevLett.129.161805)（外部サイト）
