#! /usr/bin/env bash

echo "Step1. Build Hugo with KMI env"
hugo -e kmi

echo "Step2. Upload to www.kmi.nagoya-u.ac.jp/dmunit/"
scp -rC public/* kmi:~/kmi-htdocs/dmunit/