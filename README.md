# DMUnit

- GitLab Repos: https://gitlab.com/kmipr/dmunit
- GitLab Pages: https://kmipr.gitlab.io/dmunit/
- KMI Pages: （未設定）

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Install `git` and `go`.

   ```shell
   brew install git
   brew install go
   ```

2. [Install](https://gohugo.io/getting-started/installing/) Hugo.

   ```shell
   brew install hugo
   ```

3. Fork, clone or download this project.

   ```shell
   git clone --recursive git@gitlab.com:kmipr/dmunit.git
   cd dmunit
   ```

4. Preview your project:

   ```shell
   hugo server
   ```

5. Add content.

   ```shell
   add content/セクション名/ファイル名.言語名.md
   ```

6. Optional. Generate the website:

   ```shell
   hugo
   ```

Read more at Hugo's [documentation](https://gohugo.io/getting-started/).

## Update Theme (Ananke)

1. Check update

   ```shell
   git submodule update
   git submodule status
   ```

2. Get update

   ```shell
   git submodule update --remote
   ```

3. Commit update

   ```shell
   git add themes/ananke
   git commit -m "Updated theme"
   ```

## Troubleshooting

### CSS is missing! That means two things:

- Either that you have wrongly set up the CSS URL in your templates.
- Or your static generator has a configuration option that needs to be explicitly
  set in order to serve static assets under a relative URL.

### Hugo fails to build the website

If the version of `hugo` or `hugo_extended` is 0.92.2 or later, you may have problems building the website.

Generics were introduced in [Go 1.18](https://go.dev/blog/go1.18), and they broke some features in the newer versions of Hugo. For now, if you use `hugo` or `hugo_extended` versions 0.92.2 or later, you might encounter problems building the website. To resolve the problem:

1. Edit your `.gitlab-ci.yaml` file.
1. Identify the line that declares the Hugo version.
1. Change the value to `:0.92.2`.
1. Save your changes

For more information about this issue:

- This issue is tracked in [Gitlab Hugo template fails CI/CD build with "latest" docker version](https://gitlab.com/pages/hugo/-/issues/69).
- For discussions about fixing the problem in Hugo, and proposals to potentially resolve these issues, read [proposal: spec: allow type parameters in methods](https://github.com/golang/go/issues/49085).
